package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class CompactDisc extends Disc implements Playable {
	private String artist;
	private int length;
	private List<Track> tracks = new ArrayList<Track>();

	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	public void addTrack(Track track) {
		if(!(tracks.contains(track))) {
			tracks.add(track);
			System.out.println("Da them\n");
		}
		System.out.println("Track da ton tai\n");
	}
	public void removeTrack(Track track) {
		if(tracks.contains(track)) {
			tracks.remove(track);
			System.out.println("Da xoa\n");
		}
		System.out.println("Khong co trong danh sach\n");
	}
	public int getLength() {
		int sum = 0;
		for(Track track: tracks) {
			sum +=track.getLength();
			
		}
		return sum;
		
	}
	public void play() {
		for(Track track: tracks) {
			track.play();
		}
		
	}

}
