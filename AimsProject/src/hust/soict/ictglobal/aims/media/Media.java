package hust.soict.ictglobal.aims.media;

public abstract class Media {
	private String title;
	private String category;
	private float cost;
//constructor
	public Media() {
		// TODO Auto-generated constructor stub
	}
	public Media(String title) {
		this.title=title;
	}
//
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
	

}
