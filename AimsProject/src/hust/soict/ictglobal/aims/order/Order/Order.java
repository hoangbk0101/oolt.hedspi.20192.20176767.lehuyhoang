package hust.soict.ictglobal.aims.order.Order;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hust.soict.ictglobal.aims.media.Media;

public class Order {
	public static final int MAX_NUMBER_ORDERS = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	private List<Media> itemsOrdered = new ArrayList<Media>();
	public Order() {
		super();
		nbOrders++;
	}
	public  Order createdOrder() {
		if(nbOrders < MAX_LIMITTED_ORDERS) {
			Order objOrder = new Order();
			return objOrder;
		}else {
			System.err.println("The limitted orders is almost full!");
			return null;
		}
	}
	public void addMedia(Media media) {
		if(!(itemsOrdered.contains(media))) {
			itemsOrdered.add(media);
		}
	}
	public void removeMedia(Media media) {
		if(itemsOrdered.contains(media)){
			itemsOrdered.remove(media);
		}
	}

	public float totalCost() {
		float total = 0.0f;
		Media mediaItem;
		Iterator<Media> item = itemsOrdered.iterator();
		while(item.hasNext()) {
			mediaItem = (Media) (item.next());
			total += mediaItem.getCost();
		}
		return total;
	}
	
}
